LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.all;
USE  IEEE.STD_LOGIC_ARITH.all;
USE  IEEE.STD_LOGIC_SIGNED.all;

entity timer is
	port(
		clk_50MHz					: IN std_logic;
		mode					: IN std_logic_vector(1 downto 0);
		finished				: OUT std_logic;
		output				: OUT std_logic_vector(7 downto 0)
	);
end entity;

architecture beh of timer is 

signal clk_1Hz : std_logic;

begin
	process(clk_50MHz) 
   variable clk_count : natural;
	begin
		if rising_edge(clk_50MHz) then
			clk_1Hz <= '0';
			clk_count := clk_count + 1;
				if clk_count = 50000000 then
					clk_1Hz <= '1';
					clk_count := 0;
				end if;
		end if;
	end process;

	process(clk_1Hz)
	
	variable timecount : integer range 0 to 120;
	
	begin
		if (rising_edge(clk_1Hz)) then
			if (mode = "10") then
				timecount := 120;
				finished <= '0';
			elsif (mode = "01") then
				timecount := timecount;
				finished <= '0';
			elsif (timecount > 0) AND (mode = "00") then
				timecount := timecount - 1;
				finished <= '0';
			elsif (timecount = 0) AND (mode = "00") then
				timecount := 0;
				finished <= '1';
			else
				timecount := timecount;
				finished <= '0';
			end if;
			
			output <= CONV_STD_LOGIC_VECTOR(timecount, 8);
		end if;
	end process;
end architecture;			
		