library IEEE;
use  IEEE.STD_LOGIC_1164.all;
use  IEEE.STD_LOGIC_ARITH.all;
use  IEEE.STD_LOGIC_UNSIGNED.all;

Entity dec_7seg is
port(W:in bit_vector (3 downto 0);
F:out bit_vector (6 downto 0));
end dec_7seg;

 architecture behavior of dec_7seg is
 begin
	process(W)
	begin
		case  W(3 downto 0) is
			when "0000"=> F <= not "0111111";  -- '0'
			when "0001"=> F <= not "0000110";  -- '1'
			when "0010"=> F <= not "1011011";  -- '2'
			when "0011"=> F <= not "1001111";  -- '3'
			when "0100"=> F <= not "1100110";  -- '4' 
			when "0101"=> F <= not "1101101";  -- '5'
			when "0110"=> F <= not "1111101";  -- '6'
			when "0111"=> F <= not "0000111";  -- '7'
			when "1000"=> F <= not "1111111";  -- '8'
			when "1001"=> F <= not "1100111"; -- '9'
			when "1010"=> F <= not "1110111";  -- 'A'
			when "1011"=> F <= not "1111100";  -- 'B'
			when "1100"=> F <= not "0111001";  -- 'C'
			when "1101"=> F <= not "1011110";  -- 'D'
			when "1110"=> F <= not "1111001";  -- 'E'
			when "1111"=> F <= not "1110001";  -- 'F'
	 -- Else nothing is displayed 
			when others=> F <="0000000"; 
		end case;
	end process;

 end architecture;