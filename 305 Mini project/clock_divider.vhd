library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
  
--clock divider divides the 50MHz in half to 25MHz
entity Clock_Divider is
port ( clk: in std_logic;
     clock_out: out std_logic);
end Clock_Divider;
  
architecture bhv of Clock_Divider is
  
signal tmp : std_logic := '0';
  
begin
  
process(clk)
begin
  
  if(clk'event and clk='1') then
     tmp <= NOT tmp;
  end if;
  clock_out <= tmp;
 end process;
  
end bhv;