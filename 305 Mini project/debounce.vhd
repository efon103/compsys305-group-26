LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;

ENTITY debounce IS
  GENERIC(
    counter_size  :  INTEGER := 19); --counter size (19 bits gives 10.5ms with 50MHz clock)
  PORT(
    clk     : IN  STD_LOGIC;  --input clock
    button  : IN  STD_LOGIC;  --input signal to be debounced
    result  : OUT STD_LOGIC); --debounced signal
END debounce;

ARCHITECTURE beh OF debounce IS
  SIGNAL state   : STD_LOGIC_VECTOR(1 DOWNTO 0); --input flip flops
  SIGNAL counter_set : STD_LOGIC;                    --sync reset to zero
  SIGNAL counter_out : STD_LOGIC_VECTOR(counter_size DOWNTO 0) := (OTHERS => '0'); --counter output
  signal internal : std_logic;
  signal previous : std_logic;
BEGIN

  counter_set <= state(0) xor state(1);   --determine when to start/reset counter
  
  PROCESS(clk)
  BEGIN
    IF(rising_edge(clk)) THEN
      state(0) <= button;
      state(1) <= state(0);
      If(counter_set = '1') THEN                  --reset counter because input is changing
        counter_out <= (OTHERS => '0');
      ELSIF(counter_out(counter_size) = '0') THEN --stable input time is not yet met
        counter_out <= counter_out + 1;
      ELSE                                        --stable input time is met
        internal <= state(1);
		  previous <= internal;
      END IF;

		if (previous = '1' and internal = '0') theN
			result <= '1';
		else
			result <= '0';
		end if;

    END IF;
  END PROCESS;
END ARCHITECTURE;
