LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.all;
USE  IEEE.STD_LOGIC_ARITH.all;
USE  IEEE.STD_LOGIC_SIGNED.all;

entity countdown is
	port(
		clk_50MHz			: IN std_logic;
		timerState             : IN std_logic_vector(1 downto 0);
		finished				: OUT std_logic;
		digit1_out				: OUT std_logic_vector(5 downto 0);
		digit10_out				: OUT std_logic_vector(5 downto 0)
	);
end entity;

architecture beh of countdown is 

signal digit1s: std_logic_vector(5 downto 0) := "110000";
signal digit10s: std_logic_vector(5 downto 0):= "110110";
signal clk_1Hz : std_logic;
begin

process(clk_50MHz) 
   variable clk_count : natural;
	begin
	-- make a one second clock tick
		if rising_edge(clk_50MHz) then
			clk_1Hz <= '0';
			clk_count := clk_count + 1;
				if clk_count = 50000000 then
					clk_1Hz <= '1';
					clk_count := 0;
				end if;
		end if;
	end process;

	process(timerState)
	begin
	
if(clk_1hz'event and clk_1hz = '1') then
-- if the timerState is reset,reset the countdown 
	if (timerState = "10")then
	   digit1s <= "110000";
		digit10s <= "110110";
		-- if the timerstate is run, start countind down 
	elsif(timerstate = "00")then
	  if NOT (digit1s ="110000" and digit10s = "110000")then 
		 IF(digit1s > "110000")THEN
	     digit1s <= digit1s - '1';	  
	    ELSE
		 digit10s <= digit10s - '1';
	     digit1s <= "111001";  
		 end if;
	  end if;
	end if;
	  
		 IF(digit1s ="110000" and digit10s = "110000")THEN
		 finished <= '1';
		 else
		 finished <='0';
	    END IF;
end if;
   digit1_out <= digit1s;
	digit10_out <= digit10s;
	end process;
	
end architecture;		