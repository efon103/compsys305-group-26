LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.all;

entity fsm is
	port(
		clk									: IN std_logic;
		reset_btn							: IN std_logic;
		pause_btn							: IN std_logic;
		sw										: IN std_logic_vector(3 downto 0);						
		timer_up								: IN std_logic;
		level				      			: IN std_logic;
		timer_state					 		: OUT std_logic_vector(1 downto 0); -- 00=run 01=pause 10=reset
		gameMode_out								: OUT std_logic_vector(2 downto 0)
	);
end entity;

architecture beh of fsm is
	type statetype is (initial, pause, start, finish, training, lvl1, lvl2, lvl3);
	
	signal state : statetype;


   signal gameMode								: std_logic_vector(2 downto 0);
begin
	process(clk, sw, pause_btn)
		variable state_temp : statetype;
	begin
		if (rising_edge(clk)) then
			-- Determine the next state synchronously, based on
			-- the current state and the input		
			if (reset_btn = '0') then
				state <= initial;
			else
				case state is
					when initial =>
						if (sw = "1000") OR (sw = "0100") OR (sw = "0010") OR (sw = "0001") then
							case sw is
									when "1000" => state_temp := training;
									when "0100" => state_temp := lvl1;
									when "0010" => state_temp := lvl2;
									when "0001" => state_temp := lvl3;
									when others => state_temp := initial;
							end case;
							state <= start;
						else
							state <= initial;
						end if;
					when pause =>
						if pause_btn = '1' then
							state <= pause;
						else 
							state <= state_temp;
						end if;
					when start =>
						if sw= "0000" then
							state <= state_temp;
						else 
							state <= start;
						end if;
					when finish =>
						state <= finish;
					when training =>
						if pause_btn = '1' then
							state <= pause;
							state_temp := training;
						elsif (timer_up = '1') then
						  state <= lvl1;
						else 
							state <= training;
						end if;
					when lvl1 =>
						if pause_btn = '1' then
							state <= pause;
							state_temp := lvl1;
						elsif (level = '1') then
							state <= lvl2;
						elsif (timer_up = '1') then
						   state <=finish;
						else 
							state <= lvl1;
						end if;
					when lvl2 =>
						if pause_btn = '1' then
							state <= pause;
							state_temp := lvl2;
						elsif (level = '1') then
							state <= lvl3;
						elsif (timer_up = '1') then
						   state <=finish;
						else 
							state <= lvl2;
						end if;
					when lvl3 =>
						if pause_btn = '1' then
							state <= pause;
							state_temp := lvl3;
						elsif (timer_up = '1') then
						   state <=finish;
						else 
							state <= lvl3;
						end if;
				end case;
			end if;
		end if;
	end process;
	
	-- Determine the output based only on the current state
	-- and the input (do not wait for a clock edge).
	process (state, sw, pause_btn, gameMode, level)
		   variable gameMode_temp								: std_logic_vector(2 downto 0);

	begin
		case state is
				when initial =>
						if (sw = "1000") OR (sw = "0100") OR (sw = "0010") OR (sw = "0001") then
							timer_state <= "01";
							gameMode <= "001";
							
							case sw is
								when "1000" => gameMode_temp := "010"; --training
								when "0100" => gameMode_temp := "011"; --level1
								when "0010" => gameMode_temp := "100"; --level2
								when "0001" => gameMode_temp := "101"; --level3
								when others => gameMode_temp := "000"; --initial
							end case;
						else
							timer_state <= "01";
							gameMode <= "000";
						end if;
				when pause =>
					if pause_btn = '1' then
						timer_state <= "01";
						gameMode <= "110";
						gameMode_temp := gameMode;
					else 
						timer_state <= "00";
						gameMode <= gameMode_temp;
					end if;
				when start =>
						if sw= "0000" then
							timer_state <= "10";
							gameMode <= gameMode_temp;
						else 
							timer_state <= "10";
							gameMode <= "001";
						end if;
				when finish =>
							timer_state <= "10";
							gameMode <= "111";
				when training =>
					if pause_btn = '1' then
						timer_state <= "10";
						gameMode <= "110";
					elsif (timer_up = '1') then
						  gameMode <="011";
					else 
						timer_state <= "00";
						gameMode <= "010";
					end if;
				when lvl1 =>
						if pause_btn = '1' then
							timer_state <= "10";
							gameMode <= "110";
						elsif (level = '1') then
							timer_state <= "00";
							gameMode <= "100";
						elsif (timer_up = '1') then
						  gameMode <="111";
						else 
							timer_state <= "00";
							gameMode <= "011";
						end if;
				when lvl2 =>
						if pause_btn = '1' then
							timer_state <= "10";
							gameMode <= "110";
						elsif (level = '1') then
							timer_state <= "00";
							gameMode <= "101";
						elsif (timer_up = '1') then
						  gameMode <="111";
						else 
							timer_state <= "00";
							gameMode <= "100";
						end if;
					when lvl3 =>
						if pause_btn = '1' then
							timer_state <= "10";
							gameMode <= "110";
						elsif (timer_up = '1') then
						  gameMode <="111";
						else 
							timer_state <= "00";
							gameMode <= "101";
						end if;
				end case;	
	end process;
	
	gameMode_out <= gameMode;
end architecture;