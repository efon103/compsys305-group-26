--			-- Bouncing tank2 Video 
--LIBRARY IEEE;
--USE IEEE.STD_LOGIC_1164.all;
--USE  IEEE.STD_LOGIC_ARITH.all;
--USE  IEEE.STD_LOGIC_UNSIGNED.all;
--LIBRARY lpm;
--USE lpm.lpm_components.ALL;

--PACKAGE de0core IS
--	COMPONENT vga_sync
-- 		PORT(clock_25Mhz, red, green, blue	: IN	STD_LOGIC;
--         	red_out, green_out, blue_out	: OUT 	STD_LOGIC;
--			horiz_sync_out, vert_sync_out	: OUT 	STD_LOGIC;
--			pixel_row, pixel_column			: OUT STD_LOGIC_VECTOR(9 DOWNTO 0));
--	END COMPONENT;
--END de0core;

			-- Bouncing tank2 Video 
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.all;
USE IEEE.STD_LOGIC_ARITH.all;
USE IEEE.STD_LOGIC_SIGNED.all;
LIBRARY work;
USE work.de0core.all;

ENTITY tanks IS
Generic(ADDR_WIDTH: integer := 12; DATA_WIDTH: integer := 1);

   --PORT(SIGNAL PB1, PB2, Clock 			: IN std_logic;
	PORT(SIGNAL PB1, PB2, Clock  			: IN std_logic;
        SIGNAL Red,Green,Blue 			: OUT std_logic;
        SIGNAL Horiz_sync,Vert_sync		: OUT std_logic);		
END tanks;

architecture behavior of tanks is

			-- Video Display Signals   
SIGNAL Red_Data, Green_Data, Blue_Data, vert_sync_int,
		reset, tank2_on, tank1_on, Direction			: std_logic;
SIGNAL Size 								: std_logic_vector(9 DOWNTO 0);  
SIGNAL tank2_X_motion ,tank1_X_motion						: std_logic_vector(9 DOWNTO 0);
SIGNAL tank2_Y_pos, tank2_X_pos, tank1_X_pos, tank1_Y_pos				: std_logic_vector(9 DOWNTO 0);
SIGNAL pixel_row, pixel_column				: std_logic_vector(9 DOWNTO 0); 

BEGIN           
   SYNC: vga_sync
 		PORT MAP(clock_25Mhz => clock, 
				red => red_data, green => green_data, blue => blue_data,	
    	     	red_out => red, green_out => green, blue_out => blue,
			 	horiz_sync_out => horiz_sync, vert_sync_out => vert_sync_int,
			 	pixel_row => pixel_row, pixel_column => pixel_column);

Size <= CONV_STD_LOGIC_VECTOR(10,10);
tank2_Y_pos <= CONV_STD_LOGIC_VECTOR(20,10);
tank1_Y_pos <= CONV_STD_LOGIC_VECTOR(50,10);

		-- need internal copy of vert_sync to read
vert_sync <= vert_sync_int;

		-- Colors for pixel data on video signal
Red_Data <=  '1';
		-- Turn off Green and Blue when displaying tank2
Green_Data <= NOT tank2_on;
Blue_Data <=  NOT tank2_on;

RGB_Display: Process (tank2_X_pos, tank2_Y_pos,tank1_X_pos,tank1_Y_pos, pixel_column, pixel_row, Size)
BEGIN
			-- Set tank2_on ='1' to display tank2
 IF ('0' & tank2_X_pos <= pixel_column + Size) AND
 			-- compare positive numbers only
 	(tank2_X_pos + Size >= '0' & pixel_column) AND
 	('0' & tank2_Y_pos <= pixel_row + Size) AND
 	(tank2_Y_pos + Size >= '0' & pixel_row ) THEN
 		tank2_on <= '1';
 	ELSE
 		tank2_on <= '0';
 END IF;
					-- Set tank1_on ='1' to display tank2
 IF ('0' & tank1_X_pos <= pixel_column + Size) AND
 			-- compare positive numbers only
 	(tank1_X_pos + Size >= '0' & pixel_column) AND
 	('0' & tank1_Y_pos <= pixel_row + Size) AND
 	(tank1_Y_pos + Size >= '0' & pixel_row ) THEN
 		tank1_on <= '1';
 	ELSE
 		tank1_on <= '0';
		
 END IF;
END process RGB_Display;

Move_tank2: process
BEGIN
			-- Move tank2 once every vertical sync
	WAIT UNTIL vert_sync_int'event and vert_sync_int = '1';
			-- Bounce off left or right of screen
			IF ('0' & tank2_X_pos) >= CONV_STD_LOGIC_VECTOR(640,10) - Size THEN
				tank2_X_motion <= - CONV_STD_LOGIC_VECTOR(2,10);
			ELSIF tank2_X_pos <= Size THEN
				tank2_X_motion <=  CONV_STD_LOGIC_VECTOR(2,10);
			END IF;
			-- Compute next tank2 Y position
				tank2_X_pos <= tank2_X_pos + tank2_X_motion;
END process Move_tank2;

Move_tank1: process
BEGIN
			-- Move tank1 once every vertical sync
	WAIT UNTIL vert_sync_int'event and vert_sync_int = '1';
			-- Bounce off left or right of screen
			IF ('0' & tank1_X_pos) >= CONV_STD_LOGIC_VECTOR(640,10) - Size THEN
				tank1_X_motion <= - CONV_STD_LOGIC_VECTOR(4,10);
			ELSIF tank1_X_pos <= Size THEN
				tank1_X_motion <=  CONV_STD_LOGIC_VECTOR(4,10);
			END IF;
			-- Compute next tank2 Y position
				tank1_X_pos <= tank1_X_pos + tank1_X_motion;
END process Move_tank1;

END behavior;

