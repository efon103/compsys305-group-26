LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.all;
USE  IEEE.STD_LOGIC_ARITH.all;
USE  IEEE.STD_LOGIC_SIGNED.all;

ENTITY model IS
	Generic(ADDR_WIDTH: integer := 12; DATA_WIDTH: integer := 1);
	
	--PORT(SIGNAL PB1, PB2, Clock 			: IN std_logic;
	PORT(	SIGNAL vert_sync_int			: IN std_LOGIC;
		SIGNAL MX						: IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		SIGNAL shoot					: IN std_logic;
		SIGNAL gameMode            : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
		SIGNAL random              : IN std_logic_vector(8 downto 0);
		SIGNAL BX, BY					: OUT STD_LOGIC_VECTOR(10 DOWNTO 0) := CONV_STD_LOGIC_VECTOR(0, 11);
		SIGNAL T1X, T1Y				: OUT STD_LOGIC_VECTOR(10 DOWNTO 0) := CONV_STD_LOGIC_VECTOR(0, 11);
		SIGNAL T2X, T2Y				: OUT STD_LOGIC_VECTOR(10 DOWNTO 0) := CONV_STD_LOGIC_VECTOR(0, 11);
		SIGNAL T3X, T3Y				: OUT STD_LOGIC_VECTOR(10 DOWNTO 0) := CONV_STD_LOGIC_VECTOR(0, 11);
		SIGNAL tank2_hit, tank3_hit	: OUT std_logic
	);
END model;

ARCHITECTURE behaviour of model is
	
	--Ball properties
	SIGNAL BallX : STD_LOGIC_VECTOR(10 DOWNTO 0);
	SIGNAL BallY : STD_LOGIC_VECTOR(10 DOWNTO 0) := CONV_STD_LOGIC_VECTOR(465, 11);
	SIGNAL BallY_motion		:  STD_LOGIC_VECTOR(10 DOWNTO 0);
	SIGNAL Balltemp_motion	:	STD_LOGIC_VECTOR(10 DOWNTO 0);
	
	--tank properties
	SIGNAL tank2_X_motion   : STD_LOGIC_VECTOR(10 DOWNTO 0);
	SIGNAL tank3_X_motion   : STD_LOGIC_VECTOR(10 DOWNTO 0);
	SIGNAL tank4_X_motion   : STD_LOGIC_VECTOR(10 DOWNTO 0);
	SIGNAL hit_2, hit_3     : std_logic := '0';
	SIGNAL Tank1X, Tank1Y	:  STD_LOGIC_VECTOR(10 DOWNTO 0) := CONV_STD_LOGIC_VECTOR(0, 11);
	SIGNAL Tank2X, Tank2Y	:  STD_LOGIC_VECTOR(10 DOWNTO 0) := CONV_STD_LOGIC_VECTOR(0, 11);
	SIGNAL Tank3X, Tank3Y	:  STD_LOGIC_VECTOR(10 DOWNTO 0) := CONV_STD_LOGIC_VECTOR(0, 11);
	SIGNAL Tank4X, Tank4Y	:  STD_LOGIC_VECTOR(10 DOWNTO 0) := CONV_STD_LOGIC_VECTOR(0, 11);
	--Bullet properties
	SIGNAL shoot_ball: std_logic;
	
	--size of objects
	SIGNAL ball_Size : STD_LOGIC_VECTOR(10 DOWNTO 0) := CONV_STD_LOGIC_VECTOR(4, 11);
	SIGNAL Tank_X_Size : STD_LOGIC_VECTOR(10 DOWNTO 0) := CONV_STD_LOGIC_VECTOR(24, 11);
	SIGNAL Tank_Y_Size : STD_LOGIC_VECTOR(10 DOWNTO 0) := CONV_STD_LOGIC_VECTOR(16, 11);
	
begin
	
    --set fixed y values
    Tank1Y <= CONV_STD_LOGIC_VECTOR(470, 11);
    Tank2Y <= CONV_STD_LOGIC_VECTOR(42,11);
    Tank3Y <= CONV_STD_LOGIC_VECTOR(72,11);
    Tank4Y <= CONV_STD_LOGIC_VECTOR(102,11);
	
    Tank1X <= '0' & MX; --move tank1 with mouse position
	
    Move_Ball: process(vert_sync_int)
		variable shoot_pos : std_logic_vector(10 downto 0);
		BEGIN
		-- Move ball once every vertical sync
		if (rising_edge(vert_sync_int)) then
			-- if in a game state (ie training, lvl1, lvl2, lvl3)
			IF NOT (gamemode = "110" OR gameMode = "111" OR gameMode = "001") then
			hit_2 <= '0';
			hit_3 <= '0';
			
				--when mouse is clicked, remember that the ball is shot and position it has been shot from
				IF(shoot='1' and shoot_ball = '0')THEN
					shoot_ball <= '1';
					shoot_pos := '0' & MX;
				END IF;
				
				--if ball not shot, hide ball
				IF (shoot_ball = '0') THEN
					BallY_motion <= "00000000000";
					ballX <= "01111111111";
				--otherwise, set ball position to where it was shot and set velocity
				ELSE
					ballX <= shoot_pos;
					BallY_motion <= -CONV_STD_LOGIC_VECTOR(10,11);
					BallY <= BallY + BallY_motion;
					
					--reset ball when it reaches end of screen or collides with an enemy tank
					IF(BallY <= ball_Size) THEN
						BallY <= CONV_STD_LOGIC_VECTOR(465, 11);
						BallX <= tank1X;
						shoot_ball <= '0';
					ELSIF (BallY <= Tank2Y + Tank_Y_Size) AND (BallX + ball_size <= Tank2X + Tank_X_Size) AND (BallX - ball_size >= Tank2X - tank_x_size) then
						BallY <= CONV_STD_LOGIC_VECTOR(465, 11);
						BallX <= tank1X;
						shoot_ball <='0';
					   hit_2 <= '1';
						--tank2X <= random;
					ELSIF (BallY <= Tank3Y + Tank_Y_Size) AND (BallX + ball_size <= Tank3X + Tank_X_Size) AND (BallX - ball_size >= Tank3X - tank_x_size) then
						BallY <= CONV_STD_LOGIC_VECTOR(465, 11);
						BallX <= tank1X;
						shoot_ball <='0';
						hit_3 <= '1';
						--tank3X <= random;
					END IF;
				END IF;
				END IF;
		END IF;	
	END process Move_Ball;
	
	-- controls movement of tank2
    Move_tank2: process(vert_sync_int)
	 		variable tank2_previous : std_logic_vector(10 downto 0);
			variable tank2_paused : std_logic := '0';
		BEGIN
		-- Move tank2 once every vertical sync
		if (rising_edge(vert_sync_int)) then
			IF NOT (gamemode = "000" OR gameMode = "111" OR gameMode = "001") THEN
				case gameMode is
					when "010" => -- traning
					-- Bounce off left or right of screen
					IF ('0' & Tank2X) >= CONV_STD_LOGIC_VECTOR(640,11) - Tank_X_size THEN
					   tank2_X_motion <= - CONV_STD_LOGIC_VECTOR(2,11);
					ELSIF Tank2X <= Tank_X_size THEN
						tank2_X_motion <=  CONV_STD_LOGIC_VECTOR(2,11);
					END IF;
					when "011" => -- level 1
					-- Bounce off left or right of screen
					IF ('0' & Tank2X) >= CONV_STD_LOGIC_VECTOR(640,11) - Tank_X_size THEN
					tank2_X_motion <= - CONV_STD_LOGIC_VECTOR(4,11);
					ELSIF Tank2X <= Tank_X_size THEN
						tank2_X_motion <=  CONV_STD_LOGIC_VECTOR(4,11);
					END IF;
					when "100" =>  -- level 2
					-- Bounce off left or right of screen
					IF ('0' & Tank2X) >= CONV_STD_LOGIC_VECTOR(640,11) - Tank_X_size THEN
					tank2_X_motion <= - CONV_STD_LOGIC_VECTOR(6,11);
					ELSIF Tank2X <= Tank_X_size THEN
						tank2_X_motion <=  CONV_STD_LOGIC_VECTOR(6,11);
					END IF;
					when "101" => -- level 3
					-- Bounce off left or right of screen
					IF ('0' & Tank2X) >= CONV_STD_LOGIC_VECTOR(640,11) - Tank_X_size THEN
					tank2_X_motion <= - CONV_STD_LOGIC_VECTOR(8,11);
					ELSIF Tank2X <= Tank_X_size THEN
						tank2_X_motion <=  CONV_STD_LOGIC_VECTOR(8,11);
					END IF;
					when others =>
					null;
					END case;
					--move if game not paused
					IF NOT (gamemode = "110") then
					Tank2X <= Tank2X + tank2_X_motion;
					ELSE
					Tank2X <= Tank2X;
					END IF;
			END IF;
			--generate random position if tank has been hit
			IF (BallY <= Tank2Y + Tank_Y_Size) AND (BallX + ball_size <= Tank2X + Tank_X_Size) AND (BallX - ball_size >= Tank2X - tank_x_size) then
						IF ("00" & random <= CONV_STD_LOGIC_VECTOR(64,11)) THEN
							tank2X <= "00" & random + CONV_STD_LOGIC_VECTOR(128,11);
						elsif ("00" & random >= CONV_STD_LOGIC_VECTOR(576,11)) THEN
							tank2X <= "00" & random - CONV_STD_LOGIC_VECTOR(128,11);
						else
							tank2X <= "00" & random;
						end if;
			end if;
			end if;
	END process Move_tank2;
	
	Move_tank3: process(vert_sync_int)
		variable tank3_previous : std_logic_vector(10 downto 0);
		variable tank3_paused : std_logic := '0';
		BEGIN

		-- Move tank3 once every vertical sync
		if (rising_edge(vert_sync_int)) then		
			IF NOT (gamemode = "000" OR gameMode = "111" OR gameMode = "001") THEN
				case gameMode is
					when "010" => --Traning 
					
					--tank3
					IF ('0' & Tank3X) >= CONV_STD_LOGIC_VECTOR(640,11) - Tank_X_size THEN
					tank3_X_motion <= - CONV_STD_LOGIC_VECTOR(2,11);
					ELSIF Tank3X <= Tank_X_size THEN
						tank3_X_motion <=  CONV_STD_LOGIC_VECTOR(2,11);
					END IF;
					
					when "011" => -- level 1
					
					--tank3
					IF ('0' & Tank3X) >= CONV_STD_LOGIC_VECTOR(640,11) - Tank_X_size THEN
					tank3_X_motion <= - CONV_STD_LOGIC_VECTOR(4,11);
					ELSIF Tank3X <= Tank_X_size THEN
						tank3_X_motion <=  CONV_STD_LOGIC_VECTOR(4,11);
					END IF;
					
					when "100" => -- level 2
					
					--tank3
					IF ('0' & Tank3X) >= CONV_STD_LOGIC_VECTOR(640,11) - Tank_X_size THEN
					tank3_X_motion <= - CONV_STD_LOGIC_VECTOR(8,11);
					ELSIF Tank3X <= Tank_X_size THEN
						tank3_X_motion <=  CONV_STD_LOGIC_VECTOR(8,11);
					END IF;
					
					when "101" => -- level 3
					
					--tank3
					IF ('0' & Tank3X) >= CONV_STD_LOGIC_VECTOR(640,11) - Tank_X_size THEN
					tank3_X_motion <= - CONV_STD_LOGIC_VECTOR(10,11);
					ELSIF Tank3X <= Tank_X_size THEN
						tank3_X_motion <=  CONV_STD_LOGIC_VECTOR(10,11);
					END IF;
					
					when others =>
					null;
					END case;
					-- Compute next tank3 X position
					IF NOT (gamemode = "110") then
					Tank3X <= Tank3X + tank3_X_motion;
					ELSE
					Tank3X <= Tank3X;
					END IF;
			END IF;
			
			--generate random position of tank if it has been hit
			IF (BallY <= Tank3Y + Tank_Y_Size) AND (BallX + ball_size <= Tank3X + Tank_X_Size) AND (BallX - ball_size >= Tank3X - tank_x_size) then						
						IF ("00" & random <= CONV_STD_LOGIC_VECTOR(64,11)) THEN
							tank3X <= "00" & random + CONV_STD_LOGIC_VECTOR(128,11);
						elsif ("00" & random >= CONV_STD_LOGIC_VECTOR(576,11)) THEN
							tank3X <= "00" & random - CONV_STD_LOGIC_VECTOR(128,11);
						else
							tank3X <= "00" & random;
						end if;
			end if;
		END IF;
	END process Move_tank3;
	
	
	tank2_hit <= hit_2;
	tank3_hit <= hit_3; 	
	--output new positions
	BX <= BallX;
	BY <= BallY;
	T1X <= Tank1X;
	T1Y <= Tank1Y;
	T2X <= Tank2X;
	T2Y <= Tank2Y;
	T3X <= Tank3X;
	T3Y <= Tank3Y;
END architecture;
