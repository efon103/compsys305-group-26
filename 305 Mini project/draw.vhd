LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.all;
USE  IEEE.STD_LOGIC_ARITH.all;
USE  IEEE.STD_LOGIC_SIGNED.all;
LIBRARY lpm;
USE lpm.lpm_components.ALL;

PACKAGE de0core IS
	COMPONENT vga_sync
 		PORT(clock_25Mhz : IN STD_LOGIC;
			red, green, blue	: IN	STD_LOGIC_VECTOR(3 downto 0);
         	red_out, green_out, blue_out	: OUT 	STD_LOGIC_VECTOR(3 downto 0);
			horiz_sync_out, vert_sync_out	: OUT 	STD_LOGIC;
		pixel_row_v, pixel_column_v			: OUT STD_LOGIC_VECTOR(10 downto 0));
	END COMPONENT;
	COMPONENT char_rom
 		PORT(
			character_address	:	IN STD_LOGIC_VECTOR (5 DOWNTO 0);
			font_row, font_col	:	IN STD_LOGIC_VECTOR (2 DOWNTO 0);
			clock				: 	IN STD_LOGIC ;
			
		rom_mux_output		:	OUT STD_LOGIC);
	END COMPONENT;
END de0core;

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.all;
USE IEEE.STD_LOGIC_ARITH.all;
USE IEEE.STD_LOGIC_SIGNED.all;
LIBRARY work;
USE work.de0core.all;

--draw takes the positions of the objects from the model and outputs the image to a vga display
ENTITY draw IS
	Generic(ADDR_WIDTH: integer := 12; DATA_WIDTH: integer := 1);
	
	PORT(	SIGNAL Clock : IN std_LOGIC;
		SIGNAL BX, BY: IN std_LOGIC_vector(10 downto 0); --ball position
		SIGNAL T1X, T1Y:IN std_LOGIC_vector(10 downto 0); --tank1 position
		SIGNAL T2X, T2Y: IN std_LOGIC_vector(10 downto 0); --tank2 position
		SIGNAL T3X, T3Y:IN std_LOGIC_vector(10 downto 0); --tank 3 position
		SIGNAL tank2_hit : IN STD_LOGIC;
		SIGNAL tank3_hit : IN STD_LOGIC;
		SIGNAL gamemode : IN STD_LOGIC_VECTOR(2 downto 0);
		SIGNAL digit10s, digit1s :IN std_logic_VECTOR(5 downto 0);
		SIGNAL red,green,blue 			: OUT std_logic_vector(3 downto 0);
	SIGNAL horiz_sync,vert_sync		: OUT std_logic);
END draw;

architecture behavior of draw is
	
	-- Video Display Signals   
	SIGNAL red_data, green_data, blue_data				 	: std_logic_vector(3 downto 0) := "0000";
	SIGNAL vert_sync_int, reset, Direction, text_on, ball_on		: std_logic;
	SIGNAL draw_on, t1_on, t2_on, t3_on					: std_logic := '0';
	SIGNAL tank_x_size, tank_y_size							: std_logic_vector(10 downto 0);
	SIGNAL t1_red, t1_green, t1_blue							: std_logic_vector(3 downto 0) := "0000";
	SIGNAL t2_red, t2_green, t2_blue							: std_logic_vector(3 downto 0) := "0000";
	SIGNAL t3_red, t3_green, t3_blue							: std_logic_vector(3 downto 0) := "0000";
	SIGNAL ball_size 												: std_logic_vector(10 downto 0); 
	SIGNAL ball_red, ball_green, ball_blue					: std_logic_vector(3 downto 0) := "0000";
	SIGNAL score10s, score1s			                	: std_logic_vector(5 downto 0) := "110000";
	SIGNAL text_red, text_green, text_blue					: std_logic_vector(3 downto 0) := "0000";
	SIGNAL char_address											: std_logic_vector(5 downto 0) := "100000";
	SIGNAL pixel_row, pixel_column							: std_logic_vector(10 downto 0); 

-- round ball image ROM
type rom_type is array(0 to 7) of
std_logic_vector(0 to 7);
constant BALL_ROM: rom_type:= (
"00111100",
"01111110",
"11111111",
"11111111",
"11111111",
"11111111",
"01111110",
"00111100");
signal rom_addr, rom_col: std_logic_vector(2 downto 0);
signal rom_data: std_logic_vector(7 downto 0);
signal rom_bit: std_logic;
signal ballpixel_on: std_logic;
	
BEGIN  
	
	CHAR: char_rom
	PORT MAP(clock=> clock,
		character_address=>char_address(5 downto 0),
		font_col=>pixel_column(3 downto 1),
		font_row=>pixel_row(3 downto 1),
	rom_mux_output=>text_on); 
	
	SYNC: vga_sync
	PORT MAP(clock_25Mhz => clock, 
		red => red_data, green => green_data, blue => blue_data,	
		red_out => red, green_out => green, blue_out => blue,
		horiz_sync_out => horiz_sync, vert_sync_out => vert_sync_int,
	pixel_row_v => pixel_row, pixel_column_v => pixel_column);
	
	
	tank_x_size <= CONV_STD_LOGIC_VECTOR(20,11);
	tank_y_size <= CONV_STD_LOGIC_VECTOR(10,11);
	
	--initialise tank colours
	t1_green <= "1111";
	t2_blue <= "1111";
	t3_blue <="1111";
	
	--initialise ball position and colours
	ball_size <= CONV_STD_LOGIC_VECTOR(8,11);
	ball_red <= "1111";
	
	--set text colour
	text_green <= "1000";
	text_red <= "1111";
	
	-- need internal copy of vert_sync to read
	vert_sync <= vert_sync_int;
	
				ball_on <= '1' when (BX <= pixel_column) and (pixel_column < BX + ball_Size) and
			(BY <= pixel_row) and (pixel_row < BY + ball_Size) else '0';
-- map scan coord to ROM addr/col
			rom_addr <= pixel_row(2 downto 0) - BY(2 downto 0);
			rom_col <= pixel_column(2 downto 0) - BX(2 downto 0);
			rom_data <= BALL_ROM(CONV_INTEGER('0' & rom_addr));
			rom_bit <= rom_data(CONV_INTEGER('0' & rom_col));
		
			ballpixel_on <= '1' when (ball_on = '1') and (rom_bit = '1') and
			(gamemode = "010" OR gamemode = "011" OR gamemode = "100" OR gamemode = "101") else '0';
	
	RGB_Display: Process (gamemode, BX, BY, T1X, T1Y, T2X, T2Y,T3X ,T3Y, pixel_column, pixel_row, ball_Size, tank_x_size, tank_y_size)
		BEGIN
		IF (gamemode = "010" OR gamemode = "011" OR gamemode = "100" OR gamemode = "101") then
			
			-- Set t1_on = '1' to display tank
			IF ('0' & T1X <= pixel_column + tank_x_Size) AND
			-- compare positive numbers only
			(T1X + tank_x_Size >= '0' & pixel_column) AND
			('0' & T1Y <= pixel_row + tank_y_Size) AND
			(T1Y + tank_y_Size >= '0' & pixel_row ) THEN
			t1_on <= '1';
			ELSE
				t1_on <= '0';
			END IF;
			-- Set t2_on = '1' to display tank
			IF ('0' & T2X <= pixel_column + tank_x_Size) AND
			-- compare positive numbers only
			(T2X + tank_x_Size >= '0' & pixel_column) AND
			('0' & T2Y <= pixel_row + tank_y_Size) AND
			(T2Y + tank_y_Size >= '0' & pixel_row ) THEN
			t2_on <= '1';
			ELSE
				t2_on <= '0';
			END IF;
			-- Set t3_on = '1' to display tank
			IF ('0' & T3X <= pixel_column + tank_x_Size) AND
			-- compare positive numbers only
			(T3X + tank_x_Size >= '0' & pixel_column) AND
			('0' & T3Y <= pixel_row + tank_y_Size) AND
			(T3Y + tank_y_Size >= '0' & pixel_row ) THEN
			t3_on <= '1';
			ELSE
				t3_on <= '0';
			END IF;
			ELSE
				t1_on <= '0';
				t2_on <= '0';
				t3_on <= '0';
				--ball_on <= '0';
			END IF;
	END process RGB_Display;
	
	TextDisplay: Process (pixel_column, pixel_row)
		BEGIN
		IF (gamemode = "000") THEN
			--TANKS title
			IF ((pixel_column >= CONV_STD_LOGIC_VECTOR(224,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(240,11)) AND
			(pixel_row >= CONV_STD_LOGIC_VECTOR(158,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(174,11))) THEN
				char_address <= "010100"; --T
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(240,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(256,11)) AND
			(pixel_row >= CONV_STD_LOGIC_VECTOR(158,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(174,11))) THEN
				char_address <= "000001"; --A
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(256,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(272,11)) AND
			(pixel_row >= CONV_STD_LOGIC_VECTOR(158,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(174,11))) THEN
				char_address <= "001110"; --N
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(272,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(286,11)) AND
			(pixel_row >= CONV_STD_LOGIC_VECTOR(158,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(174,11))) THEN
				char_address <= "001011"; --K
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(286,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(302,11)) AND
			(pixel_row >= CONV_STD_LOGIC_VECTOR(158,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(174,11))) THEN
				char_address <= "010011"; --S
				
				--SW0 LEVEL3
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(224,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(240,11)) AND
			(pixel_row >= CONV_STD_LOGIC_VECTOR(190,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(206,11))) THEN
				char_address <= "010011"; --S
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(240,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(256,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(190,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(206,11))) THEN
				char_address <= "010111"; --W
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(256,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(272,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(190,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(206,11))) THEN
				char_address <= "110000"; --0
				
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(286,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(302,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(190,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(206,11))) THEN
				char_address <= "001100"; --L
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(302,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(318,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(190,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(206,11))) THEN
				char_address <= "000101"; --E
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(318,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(334,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(190,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(206,11))) THEN
				char_address <= "010110"; --V
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(334,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(350,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(190,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(206,11))) THEN
				char_address <= "000101"; --E
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(350,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(366,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(190,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(206,11))) THEN
				char_address <=  "001100"; --L
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(366,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(382,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(190,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(206,11))) THEN
				char_address <= "110011"; --3
				
				--SW1 LEVEL2
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(224,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(240,11)) AND
			(pixel_row >= CONV_STD_LOGIC_VECTOR(206,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(222,11))) THEN
				char_address <= "010011"; --S
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(240,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(256,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(206,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(222,11))) THEN
				char_address <= "010111"; --W
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(256,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(272,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(206,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(222,11))) THEN
				char_address <= "110001"; --1
				
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(286,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(302,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(206,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(222,11))) THEN
				char_address <= "001100"; --L
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(302,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(318,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(206,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(222,11))) THEN
				char_address <= "000101"; --E
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(318,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(334,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(206,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(222,11))) THEN
				char_address <= "010110"; --V
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(334,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(350,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(206,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(222,11))) THEN
				char_address <= "000101"; --E
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(350,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(366,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(206,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(222,11))) THEN
				char_address <= "001100"; --L
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(366,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(382,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(206,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(222,11))) THEN
				char_address <= "110010"; --2
				
				--SW2 LEVEL1
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(224,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(240,11)) AND
			(pixel_row >= CONV_STD_LOGIC_VECTOR(222,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(238,11))) THEN
				char_address <= "010011"; --S
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(240,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(256,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(222,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(238,11))) THEN
				char_address <= "010111"; --W
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(256,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(272,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(222,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(238,11))) THEN
				char_address <= "110010"; --2
				
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(286,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(302,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(222,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(238,11))) THEN
				char_address <= "001100"; --L
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(302,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(318,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(222,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(238,11))) THEN
				char_address <= "000101"; --E
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(318,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(334,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(222,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(238,11))) THEN
				char_address <= "010110"; --V
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(334,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(350,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(222,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(238,11))) THEN
				char_address <= "000101"; --E
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(350,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(366,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(222,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(238,11))) THEN
				char_address <= "001100"; --L
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(366,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(382,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(222,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(238,11))) THEN
				char_address <= "110001"; --1
				
				--SW3 TRAINING
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(224,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(240,11)) AND
			(pixel_row >= CONV_STD_LOGIC_VECTOR(238,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(254,11))) THEN
				char_address <= "010011"; --S
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(240,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(256,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(238,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(254,11))) THEN
				char_address <= "010111"; --W
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(256,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(272,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(238,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(254,11))) THEN
				char_address <= "110011"; --3
				
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(286,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(302,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(238,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(254,11))) THEN
				char_address <= "010100"; --T
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(302,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(318,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(238,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(254,11))) THEN
				char_address <= "010010"; --R
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(318,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(334,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(238,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(254,11))) THEN
				char_address <= "000001"; --A
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(334,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(350,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(238,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(254,11))) THEN
				char_address <= "001001"; --I
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(350,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(366,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(238,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(254,11))) THEN
				char_address <= "001110"; --N
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(366,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(382,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(238,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(254,11))) THEN
				char_address <= "001001"; --I
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(382,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(398,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(238,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(254,11))) THEN
				char_address <= "001110"; --N
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(398,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(414,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(238,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(254,11))) THEN
				char_address <= "000111"; --G	
				
				--SW4 PAUSED
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(224,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(240,11)) AND
			(pixel_row >= CONV_STD_LOGIC_VECTOR(254,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(270,11))) THEN
				char_address <= "010011"; --S
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(240,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(256,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(254,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(270,11))) THEN
				char_address <= "010111"; --W
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(256,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(272,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(254,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(270,11))) THEN
				char_address <= "110100"; --4
				
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(286,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(302,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(254,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(270,11))) THEN
				char_address <= "010000"; --P
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(302,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(318,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(254,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(270,11))) THEN
				char_address <= "000001"; --A
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(318,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(334,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(254,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(270,11))) THEN
				char_address <= "010101"; --U
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(334,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(350,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(254,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(270,11))) THEN
				char_address <= "010011"; --S
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(350,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(366,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(254,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(270,11))) THEN
				char_address <= "000101"; --E
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(366,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(382,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(254,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(270,11))) THEN
			char_address <= "000100"; --D	
			ELSE
				char_address <="100000";
			END IF;
			
		--BEGIN SCREEN
		ELSIF (gamemode = "001") then
			--TO BEGIN SW3-0 OFF
			IF ((pixel_column >= CONV_STD_LOGIC_VECTOR(224,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(240,11)) AND
			(pixel_row >= CONV_STD_LOGIC_VECTOR(206,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(222,11))) THEN
				char_address <= "010100"; --T
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(240,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(256,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(206,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(222,11))) THEN
				char_address <= "001111"; --O
				
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(272,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(286,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(206,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(222,11))) THEN
				char_address <= "000010"; --B
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(286,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(302,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(206,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(222,11))) THEN
				char_address <= "000101"; --E
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(302,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(318,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(206,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(222,11))) THEN
				char_address <= "000111"; --G
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(318,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(334,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(206,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(222,11))) THEN
				char_address <= "001001"; --I
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(334,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(350,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(206,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(222,11))) THEN
				char_address <= "001110"; --N
				
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(224,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(240,11)) AND
			(pixel_row >= CONV_STD_LOGIC_VECTOR(254,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(270,11))) THEN
				char_address <= "010011"; --S
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(240,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(256,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(254,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(270,11))) THEN
				char_address <= "010111"; --W
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(256,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(272,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(254,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(270,11))) THEN
				char_address <= "110011"; --3
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(272,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(286,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(254,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(270,11))) THEN
				char_address <= "101101"; --
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(286,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(302,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(254,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(270,11))) THEN
				char_address <= "110000"; --0
				
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(318,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(334,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(254,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(270,11))) THEN
				char_address <= "001111"; --O
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(334,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(350,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(254,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(270,11))) THEN
				char_address <= "000110"; --F
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(350,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(366,11)) AND
	        (pixel_row >= CONV_STD_LOGIC_VECTOR(254,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(270,11))) THEN
			char_address <= "000110"; --F
			ELSE
				char_address <= "100000";
			END IF;
		
		ELSIF (gamemode = "010") then
			IF ((pixel_column >= CONV_STD_LOGIC_VECTOR(0,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(15,11)) AND
			(pixel_row >= CONV_STD_LOGIC_VECTOR(0,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(15,11))) THEN
				char_address <= "010011"; -- S
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(15,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(31,11)) AND
			(pixel_row >= CONV_STD_LOGIC_VECTOR(0,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(15,11))) THEN
				char_address <= "000011"; -- C
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(31,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(47,11)) AND
			(pixel_row >= CONV_STD_LOGIC_VECTOR(0,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(15,11))) THEN
				char_address <= "001111"; -- O
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(47,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(63,11)) AND
			(pixel_row >= CONV_STD_LOGIC_VECTOR(0,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(15,11))) THEN
				char_address <= "010010"; -- R
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(63,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(79,11)) AND
			(pixel_row >= CONV_STD_LOGIC_VECTOR(0,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(15,11))) THEN
				char_address <= "000101"; -- E
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(95,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(111,11)) AND
			(pixel_row >= CONV_STD_LOGIC_VECTOR(0,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(15,11))) THEN
				char_address <= score10s;
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(111,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(127,11)) AND
			(pixel_row >= CONV_STD_LOGIC_VECTOR(0,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(15,11))) THEN
				char_address <= score1s;
				-- TIME: Text Display	
			ELSIF ((pixel_column >= "00000000000") AND (pixel_column <= "00000001111") AND
			(pixel_row >= "001111") AND (pixel_row <= "011111")) THEN
				char_address <= "010100"; -- T
			ELSIF ((pixel_column >= "00000001111") AND (pixel_column <= "00000011111") AND
			(pixel_row >= "001111") AND (pixel_row <= "011111")) THEN
				char_address <= "001001"; -- I
			ELSIF ((pixel_column >= "00000011111") AND (pixel_column <= "00000101110") AND
			(pixel_row >= "001111") AND (pixel_row <= "011111")) THEN
				char_address <= "001101"; -- M
			ELSIF ((pixel_column >= "00000101110") AND (pixel_column <= "00000111110") AND
			(pixel_row >= "001111") AND (pixel_row <= "011111")) THEN
				char_address <= "000101"; -- E
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(95,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(111,11)) AND
			(pixel_row >= "001111") AND (pixel_row <= "011111")) THEN
				char_address <= digit10s;  --time10
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(111,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(127,11)) AND
			(pixel_row >= "001111") AND (pixel_row <= "011111")) THEN
				char_address <= digit1s;   --time1
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(479,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(495,11)) AND
			(pixel_row >= "001111") AND (pixel_row <= "011111")) THEN
				char_address <= "010100"; --T
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(495,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(511,11)) AND
			(pixel_row >= "001111") AND (pixel_row <= "011111")) THEN
				char_address <= "010010"; --R
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(511,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(527,11)) AND
			(pixel_row >= "001111") AND (pixel_row <= "011111")) THEN
				char_address <= "000001"; --A
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(527,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(543,11)) AND
			(pixel_row >= "001111") AND (pixel_row <= "011111")) THEN
				char_address <= "001001"; --I
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(543,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(559,11)) AND
			(pixel_row >= "001111") AND (pixel_row <= "011111")) THEN
				char_address <= "001110"; --N
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(559,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(575,11)) AND
			(pixel_row >= "001111") AND (pixel_row <= "011111")) THEN
				char_address <= "001001"; --I
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(575,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(591,11)) AND
			(pixel_row >= "001111") AND (pixel_row <= "011111")) THEN
				char_address <= "001110"; --N
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(591,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(607,11)) AND
			(pixel_row >= "001111") AND (pixel_row <= "011111")) THEN
			char_address <= "000111"; --G
			ELSE
				char_address <= "100000";
			END IF;	
		--Text Display for Training, Level 1, 2, and 3
		ELSIF(gamemode = "011") OR (gamemode = "100") OR (gamemode = "101") then
			-- SCOORRRREEEEEEEE Text Display
			IF ((pixel_column >= CONV_STD_LOGIC_VECTOR(0,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(15,11)) AND
			(pixel_row >= CONV_STD_LOGIC_VECTOR(0,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(15,11))) THEN
				char_address <= "010011"; -- S
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(15,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(31,11)) AND
			(pixel_row >= CONV_STD_LOGIC_VECTOR(0,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(15,11))) THEN
				char_address <= "000011"; -- C
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(31,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(47,11)) AND
			(pixel_row >= CONV_STD_LOGIC_VECTOR(0,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(15,11))) THEN
				char_address <= "001111"; -- O
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(47,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(63,11)) AND
			(pixel_row >= CONV_STD_LOGIC_VECTOR(0,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(15,11))) THEN
				char_address <= "010010"; -- R
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(63,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(79,11)) AND
			(pixel_row >= CONV_STD_LOGIC_VECTOR(0,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(15,11))) THEN
				char_address <= "000101"; -- E
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(95,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(111,11)) AND
			(pixel_row >= CONV_STD_LOGIC_VECTOR(0,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(15,11))) THEN
				char_address <= score10s;
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(111,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(127,11)) AND
			(pixel_row >= CONV_STD_LOGIC_VECTOR(0,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(15,11))) THEN
				char_address <= score1s;
				-- TIME: Text Display	
			ELSIF ((pixel_column >= "00000000000") AND (pixel_column <= "00000001111") AND
			(pixel_row >= "001111") AND (pixel_row <= "011111")) THEN
				char_address <= "010100"; -- T
			ELSIF ((pixel_column >= "00000001111") AND (pixel_column <= "00000011111") AND
			(pixel_row >= "001111") AND (pixel_row <= "011111")) THEN
				char_address <= "001001"; -- I
			ELSIF ((pixel_column >= "00000011111") AND (pixel_column <= "00000101110") AND
			(pixel_row >= "001111") AND (pixel_row <= "011111")) THEN
				char_address <= "001101"; -- M
			ELSIF ((pixel_column >= "00000101110") AND (pixel_column <= "00000111110") AND
			(pixel_row >= "001111") AND (pixel_row <= "011111")) THEN
				char_address <= "000101"; -- E
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(95,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(111,11)) AND
			(pixel_row >= "001111") AND (pixel_row <= "011111")) THEN
				char_address <= digit10s;  --time10
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(111,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(127,11)) AND
			(pixel_row >= "001111") AND (pixel_row <= "011111")) THEN
				char_address <= digit1s;   --time1
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(511,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(527,11)) AND
			(pixel_row >= "001111") AND (pixel_row <= "011111")) THEN
				char_address <= "001100"; -- L
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(527,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(543,11)) AND
			(pixel_row >= "001111") AND (pixel_row <= "011111")) THEN
				char_address <= "000101"; -- E
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(543,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(559,11)) AND
			(pixel_row >= "001111") AND (pixel_row <= "011111")) THEN
				char_address <= "010110"; -- V
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(559,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(575,11)) AND
			(pixel_row >= "001111") AND (pixel_row <= "011111")) THEN
				char_address <= "000101"; -- E
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(575,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(591,11)) AND
			(pixel_row >= "001111") AND (pixel_row <= "011111")) THEN
				char_address <= "001100"; -- L
			ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(591,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(607,11)) AND
			(pixel_row >= "001111") AND (pixel_row <= "011111")) THEN
				--Display Level Number
				case gamemode is
					when "011" => char_address <= "110001";
					when "100" => char_address <= "110010";
					when "101" => char_address <= "110011";
					when others => char_address <= "100000";
				end case;
				ELSE
					char_address <= "100000";
				END IF;
	--PAUSED
	ELSIF (gamemode = "110") THEN
		IF ((pixel_column >= CONV_STD_LOGIC_VECTOR(272,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(288,11)) AND
		(pixel_row >= CONV_STD_LOGIC_VECTOR(238,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(254,11))) THEN
		char_address <= "010000"; --P
		ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(288,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(304,11)) AND
		(pixel_row >= CONV_STD_LOGIC_VECTOR(238,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(254,11))) THEN
		char_address <= "000001"; --A
		ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(304,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(320,11)) AND
		(pixel_row >= CONV_STD_LOGIC_VECTOR(238,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(254,11))) THEN
		char_address <= "010101"; --U
		ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(320,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(336,11)) AND
		(pixel_row >= CONV_STD_LOGIC_VECTOR(238,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(254,11))) THEN
		char_address <= "010011"; --S
		ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(336,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(352,11)) AND
		(pixel_row >= CONV_STD_LOGIC_VECTOR(238,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(254,11))) THEN
		char_address <= "000101"; --E
		ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(352,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(368,11)) AND
		(pixel_row >= CONV_STD_LOGIC_VECTOR(238,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(254,11))) THEN
		char_address <= "000100"; --D
		ELSE
		char_address <= "100000";
		END IF;
		
	--FINISHED
	ELSIF (gamemode = "111") THEN
		IF ((pixel_column >= CONV_STD_LOGIC_VECTOR(224,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(240,11)) AND
		(pixel_row >= CONV_STD_LOGIC_VECTOR(206,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(222,11))) THEN
		char_address <= "000110"; --F
		ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(240,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(256,11)) AND
		(pixel_row >= CONV_STD_LOGIC_VECTOR(206,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(222,11))) THEN
		char_address <= "001001"; --I
		ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(256,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(272,11)) AND
		(pixel_row >= CONV_STD_LOGIC_VECTOR(206,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(222,11))) THEN
		char_address <= "001110"; --N
		ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(272,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(288,11)) AND
		(pixel_row >= CONV_STD_LOGIC_VECTOR(206,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(222,11))) THEN
		char_address <= "001001"; --I
		ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(288,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(304,11)) AND
		(pixel_row >= CONV_STD_LOGIC_VECTOR(206,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(222,11))) THEN
		char_address <= "010011"; --S
		ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(304,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(320,11)) AND
		(pixel_row >= CONV_STD_LOGIC_VECTOR(206,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(222,11))) THEN
		char_address <= "001000"; --H
		ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(320,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(336,11)) AND
		(pixel_row >= CONV_STD_LOGIC_VECTOR(206,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(222,11))) THEN
		char_address <= "000101"; --E
		ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(336,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(352,11)) AND
		(pixel_row >= CONV_STD_LOGIC_VECTOR(206,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(222,11))) THEN
		char_address <= "000100"; --D
		--Final Score
		ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(224,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(240,11)) AND
		(pixel_row >= CONV_STD_LOGIC_VECTOR(222,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(238,11))) THEN
		char_address <= "010011"; -- S
		ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(240,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(256,11)) AND
		(pixel_row >= CONV_STD_LOGIC_VECTOR(222,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(238,11))) THEN
		char_address <= "000011"; -- C
		ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(256,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(272,11)) AND
		(pixel_row >= CONV_STD_LOGIC_VECTOR(222,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(238,11))) THEN
		char_address <= "001111"; -- O
		ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(272,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(288,11)) AND
		(pixel_row >= CONV_STD_LOGIC_VECTOR(222,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(238,11))) THEN
		char_address <= "010010"; -- R
		ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(288,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(304,11)) AND
		(pixel_row >= CONV_STD_LOGIC_VECTOR(222,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(238,11))) THEN
		char_address <= "000101"; -- E
		ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(304,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(320,11)) AND
		(pixel_row >= CONV_STD_LOGIC_VECTOR(222,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(238,11))) THEN
		char_address <= score10s;
		ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(320,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(336,11)) AND
		(pixel_row >= CONV_STD_LOGIC_VECTOR(222,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(238,11))) THEN
		char_address <= score1s;
		--BT1 RESTART
		ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(224,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(240,11)) AND
		(pixel_row >= CONV_STD_LOGIC_VECTOR(238,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(254,11))) THEN
		char_address <= "000010"; --B
		ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(240,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(256,11)) AND
		(pixel_row >= CONV_STD_LOGIC_VECTOR(238,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(254,11))) THEN
		char_address <= "010100"; --T
		ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(256,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(272,11)) AND
		(pixel_row >= CONV_STD_LOGIC_VECTOR(238,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(254,11))) THEN
		char_address <= "110001"; --1
		ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(288,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(304,11)) AND
		(pixel_row >= CONV_STD_LOGIC_VECTOR(238,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(254,11))) THEN
		char_address <= "010010"; --R
		ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(304,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(320,11)) AND
		(pixel_row >= CONV_STD_LOGIC_VECTOR(238,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(254,11))) THEN
		char_address <= "000101"; --E
		ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(320,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(336,11)) AND
		(pixel_row >= CONV_STD_LOGIC_VECTOR(238,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(254,11))) THEN
		char_address <= "010011"; --S
		ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(336,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(352,11)) AND
		(pixel_row >= CONV_STD_LOGIC_VECTOR(238,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(254,11))) THEN
		char_address <= "010100"; --T
		ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(352,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(368,11)) AND
		(pixel_row >= CONV_STD_LOGIC_VECTOR(238,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(254,11))) THEN
		char_address <= "000001"; --A
		ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(368,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(384,11)) AND
		(pixel_row >= CONV_STD_LOGIC_VECTOR(238,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(254,11))) THEN
		char_address <= "010010"; --R
		ELSIF ((pixel_column >= CONV_STD_LOGIC_VECTOR(384,11)) AND (pixel_column <= CONV_STD_LOGIC_VECTOR(400,11)) AND
		(pixel_row >= CONV_STD_LOGIC_VECTOR(238,11)) AND (pixel_row <= CONV_STD_LOGIC_VECTOR(254,11))) THEN
		char_address <= "010100"; --T
	ELSE
		char_address <= "100000";
	END IF;
	
	ELSE
		char_address <= "100000";
	END IF;
	END process TextDisplay;
	
	Score_Display: Process
	BEGIN
	WAIT UNTIL vert_sync_int'event and vert_sync_int = '1';
	IF (gamemode ="000") then
	score1s <= "110000";
	score10s <= "110000";
	ELSIF NOT(gamemode ="000") then
	IF(tank2_hit ='1' OR tank3_hit ='1')THEN
	IF(score1s < "111001")THEN
	score1s <= score1s + '1';
	ELSE
	score10s <= score10s + '1';
	score1s <="110000";
	END IF;
	END IF;
	END IF;
	END PROCESS Score_Display; 
	
	--output according to hierachy
	red_data <= 
	text_red when text_on = '1' else
	
	t1_red when t1_on = '1' else
	t2_red when t2_on = '1' else
	t3_red when t3_on = '1' else
	ball_red when ballpixel_on = '1' else
	"0000" when gamemode = "110" OR gamemode = "000" OR gamemode = "001" else
	"0001";
	
	green_data <= 
	text_green when text_on='1' else
	
	t1_green when t1_on = '1' else
	t2_green when t2_on = '1' else
	t3_green when t3_on = '1' else
	ball_green when ballpixel_on = '1' else
	"0000" when gamemode = "110" OR gamemode = "000" OR gamemode = "001" else
	"0010";
	
	blue_data <= 
	text_blue when text_on='1' else
	
	t1_blue when t1_on = '1' else
	t2_blue when t2_on = '1' else
	t3_blue when t3_on = '1' else
	ball_blue when ballpixel_on = '1' else
	"0000" when gamemode = "110" OR gamemode = "000" OR gamemode = "001" else
	"0001";
	
	END behavior;
	
		