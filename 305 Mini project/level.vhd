library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
USE  IEEE.STD_LOGIC_UNSIGNED.all;

entity level is
port(
	clk				: IN STD_LOGIC;
	gamemode				: IN STD_LOGIC_VECTOR(2 downto 0);
	tank2_hit			: IN STD_LOGIC;
	tank3_hit			: IN STD_LOGIC;
	level_up			: OUT STD_LOGIC
	);
end entity;

architecture beh of level is
SIGNAL hits_count : std_logic_vector(4 downto 0) :="00000";

begin
process(clk)
begin
-- counting the number of hits
	if (rising_edge(clk)) then
		IF (gamemode = "010" OR gamemode = "011" OR gamemode = "100" OR gamemode = "101") THEN
		 IF(tank2_hit ='1' OR tank3_hit ='1') THEN
			 hits_count <= hits_count + '1';
		 END IF;
	 -- if 10 hits and in level 1, level up to level 2
		 IF (hits_count = "01010" AND gamemode = "011") THEN
		 level_up <= '1';
	-- if 20 hits and in level 2, level up to level 3, else reset level_up signal 	 
		 ELSIF  (hits_count = "10100" AND gamemode = "100")THEN
		 level_up <= '1';
		 ELSE
		 level_up <='0';
		 END IF;
	-- if in traning mode, no level up.	 
		 ELSIF (gamemode = "110") then
			level_up <= '0';
		ELSE
			level_up <= '0';
			hits_count <= "00000";
		end if;
	END IF;
	 
	END PROCESS;
END ARCHITECTURE;