library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity pseudorng is
Port ( clock : in STD_LOGIC;
       Q : out STD_LOGIC_VECTOR (8 downto 0));

end pseudorng;

architecture Behavioral of pseudorng is

signal Qt: STD_LOGIC_VECTOR(8 downto 0) := "000000001";

begin

PROCESS(clock)
variable tmp : STD_LOGIC := '0';
BEGIN

IF rising_edge(clock) THEN
      tmp := Qt(4) XOR Qt(3) XOR Qt(2) XOR Qt(0);
      Qt <= tmp & Qt(8 downto 1);
END IF;
END PROCESS;

Q <= Qt;

end Behavioral;