library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
USE  IEEE.STD_LOGIC_UNSIGNED.all;

--hitToScore counts the number of hits and separates the value into 10s and 1s
entity hitToScore is
port(
	clk				: IN STD_LOGIC;
	tank2_hit			: IN STD_LOGIC;
	tank3_hit			: IN STD_LOGIC;
	gamemode			: IN STD_LOGIC_VECTOR(2 downto 0);
	output1s		: OUT STD_LOGIC_VECTOR(3 downto 0);
	output10s			: OUT STD_LOGIC_VECTOR(3 downto 0)
	);
end entity;

architecture beh of hitToScore is
SIGNAL output10s_temp : STD_LOGIC_VECTOR(3 downto 0);
SIGNAL output1s_temp : STD_LOGIC_VECTOR(3 downto 0);

begin
process(clk)
begin
	if (rising_edge(clk)) then
		--reset count when in main menu
		IF (gamemode = "000") THEN
			output10s_temp <= "0000";
			output1s_temp <= "0000";
		--do nothing on pause
		ELSIF (gamemode = "110") THEN
			null;
		ELSE
		--otherwise, count if tank hits
			IF(tank2_hit ='1' OR tank3_hit ='1') THEN
				IF(output1s_temp < "1001")THEN
					output1s_temp <= output1s_temp + "0001";
				ELSE
					output10s_temp <= output10s_temp + "0001";
					output1s_temp <= "0000";
				END IF;
			END IF;
	END IF;
		output10s <= output10s_temp;
		output1s <= output1s_temp; 
END IF;
	END PROCESS;
END ARCHITECTURE;
	